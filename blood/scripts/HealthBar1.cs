﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar1 : MonoBehaviour{
    public float curBlood = 10f;
    private float targetBlood = 10f;
    private Rect bloodBarArea;
    private Rect addButton;
    private Rect minusButton;
    private Rect num1;
    private Rect num2;
    private Rect num3;
    private Rect percent;

    void Start () {
        bloodBarArea = new Rect(Screen.width - 220, 20, 200, 50);
        addButton = new Rect(Screen.width - 220, 50, 40, 20);
        minusButton = new Rect(Screen.width - 60, 50, 40, 20);

        num1 = new Rect(Screen.width - 220, 80, 40, 20);
        num2 = new Rect(Screen.width - 140, 80, 40, 20);
        num3 = new Rect(Screen.width - 60, 80, 40, 20);

        percent = new Rect(Screen.width - 300, 20, 60, 20);
	}

    public void setBlood(float num) {
        targetBlood = 10.0f * num;
    }

    private void OnGUI() {
        if (GUI.Button(addButton, " + ")) {
            targetBlood = targetBlood + 1.0f > 10.0f ? 10.0f : targetBlood + 1.0f;
        }
            
            
        if (GUI.Button(minusButton, " - ")) {
            targetBlood = targetBlood - 1.0f < 0.0f ? 0.0f : targetBlood - 1.0f;
        }

        if (GUI.Button(num1, " 25% ")) 
            setBlood(0.25f);
        if (GUI.Button(num2, " 50% "))
            setBlood(0.5f);
        if (GUI.Button(num3, " 75% "))
            setBlood(0.75f);
        
        curBlood = Mathf.Lerp(curBlood, targetBlood, 0.1f);
        GUI.HorizontalScrollbar(bloodBarArea, 0f, curBlood, 0f, 10f);
        GUI.TextField(percent, (targetBlood / 10.0f).ToString("P2"));
    }
}
