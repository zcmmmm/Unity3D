### 09-UI系统

[对应博客](https://blog.csdn.net/try17875864815/article/details/110880074)

**使用说明**

> IMGUI版本：将`HealthBar1.cs` 挂在到新建的空的GameObject运行即可
> - 点击`+`/`-`每次增加/减少10%的血量
> - 点击对应数值设置血量到对应百分比
> - 血条UI左边的百分数为当前剩余血量百分比

> UGUI版本：按照对应博客构建好文件之后将`HealthBar2.cs`挂载到Canvas即可

**预制的使用方法**

1. IMGUI版本：将perfabs/GameObject直接拖入场景点击运行即可

2. UGUI版本：将perfabs/Ethan直接拖入场景，并创建一个Plane（位置为(0, 0, 0)），点击运行即可
