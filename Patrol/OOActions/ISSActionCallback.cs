﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum SSActionEventType : int { Started, Completed }
// 正常动作/追捕动作
public enum SSActionTargetType : int { Normal, Catching }    

public interface ISSActionCallback {
    void SSActionEvent(SSAction source,
        SSActionEventType eventType = SSActionEventType.Completed,
        // 动作结束时回调，告知动作类型
        SSActionTargetType intParam = SSActionTargetType.Normal,    
        string strParam = null,
        Object objParam = null);
}
