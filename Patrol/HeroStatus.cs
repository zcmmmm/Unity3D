﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.Patrols;

public class HeroStatus : MonoBehaviour {
    public int standOnArea = -1;

	void Start () {
		
	}
	
	void Update () {
        modifyStandOnArea();
	}

    // 检测hero所在区域
    void modifyStandOnArea() {
        float posX = this.gameObject.transform.position.x;
        float posZ = this.gameObject.transform.position.z;
        // Debug.Log(posX + " " +  posZ);
        if (posZ >= FenchLocation.FenchHori) {
            if (posX < FenchLocation.FenchVertLeft)
                standOnArea = 0;
            else if (posX > FenchLocation.FenchVertRight)
                standOnArea = 2;
            else if (posX >= FenchLocation.FenchVertLeft && posX <= FenchLocation.FenchVertRight)
                standOnArea = 1;
            else
                standOnArea = -1;
        }
        else {
            if (posX < FenchLocation.FenchVertLeft)
                standOnArea = 3;
            else if (posX > FenchLocation.FenchVertRight)
                standOnArea = 5;
            else if (posX >= FenchLocation.FenchVertLeft && posX <= FenchLocation.FenchVertRight)
                standOnArea = 4;
            else
                standOnArea = -1;
        }
    }
}
