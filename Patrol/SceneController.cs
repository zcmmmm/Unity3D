﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Patrols {
    public class Diretion {
        public const int UP = 0;
        public const int DOWN = 2;
        public const int LEFT = -1;
        public const int RIGHT = 1;
    }

    public class FenchLocation {
        public const float FenchTop = 20.0f;
        public const float FenchBottom = 4.0f;
        public const float FenchLeft = -9.0f;
        public const float FenchRight = 9.0f;
        public const float FenchHori = 12.42f;
        public const float FenchVertLeft = -3.0f;
        public const float FenchVertRight = 3.0f;
    }

    public class Hole {
        public float x1;
        public float x2;
        public float y1;
        public float y2;
        public Hole(float xx1, float xx2, float yy1, float yy2) {
            x1 = xx1;
            x2 = xx2;
            y1 = yy1;
            y2 = yy2;
        }

        public bool isInHole(float dx, float dy) {
            if(x1 <= dx && dx <= x2 && y1 <= dy && dy <= y2)
                return true;
            return false;
        }
    }

    public class Holes {
        public Hole leftUpHole;
        public Hole leftMidHole;
        public Hole leftDownHole;
        public Hole MidHole;
        public Hole rightUpHole;
        public Hole rightMidHole;
        public Hole rightDownHole;
    }

    public interface IUserAction {
        void heroMove(int dir);
    }

    public interface IAddAction {
        void addRandomMovement(GameObject sourceObj, bool isActive);
        void addDirectMovement(GameObject sourceObj);
    }

    public interface IGameStatusOp {
        int getHeroStandOnArea();
        void heroEscapeAndScore();
        void patrolHitHeroAndGameover();
    }

    public class SceneController : System.Object, IUserAction, IAddAction, IGameStatusOp {
        private static SceneController instance;
        private GameModel myGameModel;
        private GameEventManager myGameEventManager;

        public static SceneController getInstance() {
            if (instance == null)
                instance = new SceneController();
            return instance;
        }

        internal void setGameModel(GameModel _myGameModel) {
            if (myGameModel == null) {
                myGameModel = _myGameModel;
            }
        }

        internal void setGameEventManager(GameEventManager _myGameEventManager) {
            if (myGameEventManager == null) {
                myGameEventManager = _myGameEventManager;
            }
        }

        /*********************实现IUserAction接口*********************/
        public void heroMove(int dir) {
            myGameModel.heroMove(dir);
        }

        /*********************实现IAddAction接口*********************/
        public void addRandomMovement(GameObject sourceObj, bool isActive) {
            myGameModel.addRandomMovement(sourceObj, isActive);
        }

        public void addDirectMovement(GameObject sourceObj) {
            myGameModel.addDirectMovement(sourceObj);
        }

        /*********************实现IGameStatusOp接口*********************/
        public int getHeroStandOnArea() {
            return myGameModel.getHeroStandOnArea();
        }

        public void heroEscapeAndScore() {
            myGameEventManager.heroEscapeAndScore();
        }

        public void patrolHitHeroAndGameover() {
            myGameEventManager.patrolHitHeroAndGameover();
        }
    }
}

