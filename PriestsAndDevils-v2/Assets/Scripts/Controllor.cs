﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controllor : MonoBehaviour, ISceneController, IUserAction
{
    public LandModel start_land;            // 开始陆地
    public LandModel end_land;              // 结束陆地
    public BoatModel boat;                  // 船
    private Judger judger;                  // 裁判员-动作分离版
    private RoleModel[] roles;              // 角色
    UserGUI user_gui;

    public MySceneActionManager actionManager;   // 创建动作管理器

    void Start () {
        SSDirector director = SSDirector.GetInstance();
        director.CurrentScenceController = this;
        user_gui = gameObject.AddComponent<UserGUI>() as UserGUI;
        // 创建裁判-动作分离版
        LoadResources();

        // 动作管理器
        actionManager = gameObject.AddComponent<MySceneActionManager>() as MySceneActionManager;    
    }
	
    public void LoadResources() {             // 创建水，陆地，角色，船，裁判员
        GameObject water = Instantiate(Resources.Load("Prefabs/Water", typeof(GameObject)), new Vector3(0, -1, 30), Quaternion.identity) as GameObject;
        water.name = "water";       
        start_land = new LandModel("start");
        end_land = new LandModel("end");
        boat = new BoatModel();
        roles = new RoleModel[6];

        for (int i = 0; i < 3; i++) {
            RoleModel role = new RoleModel("priest");
            role.SetName("priest" + i);
            role.SetPosition(start_land.GetEmptyPosition());
            role.GoLand(start_land);
            start_land.AddRole(role);
            roles[i] = role;
        }

        for (int i = 0; i < 3; i++) {
            RoleModel role = new RoleModel("devil");
            role.SetName("devil" + i);
            role.SetPosition(start_land.GetEmptyPosition());
            role.GoLand(start_land);
            start_land.AddRole(role);
            roles[i + 3] = role;
        }

        judger = new Judger(start_land, end_land, boat);
    }

    public void MoveBoat() {                 //移动船
        if (boat.IsEmpty() || user_gui.sign != 1) 
            return;
        // boat.BoatMove();
        // 移动船-动作分离版
        actionManager.moveBoat(boat.getGameObject(), boat.BoatMoveToPosition(), boat.move_speed);
    }

    public void MoveRole(RoleModel role) {   // 移动角色
        if (user_gui.sign != 1) 
            return;

        if (role.IsOnBoat()) {               // 游戏对象在船上
            LandModel land;
            if (boat.GetBoatSign() == -1)
                land = end_land;
            else
                land = start_land;
            boat.DeleteRoleByName(role.GetName());
            // role.Move(land.GetEmptyPosition());
            // 移动人物-动作分离版
            Vector3 end_pos = land.GetEmptyPosition();
            Vector3 middle_pos = new Vector3(role.getGameObject().transform.position.x, end_pos.y, end_pos.z);
            actionManager.moveRole(role.getGameObject(), middle_pos, end_pos, role.move_speed);
            
            role.GoLand(land);
            land.AddRole(role);
        } else {                           // 游戏对象不在船上
            LandModel land = role.GetLandModel();
            // 船没有空位，也不是船停靠的陆地，就不上船
            if (boat.GetEmptyNumber() == -1 || land.GetLandSign() != boat.GetBoatSign()) 
                return;   

            land.DeleteRoleByName(role.GetName());
            // role.Move(boat.GetEmptyPosition());
            // 移动人物-动作分离版
            Vector3 end_pos = boat.GetEmptyPosition();
            Vector3 middle_pos = new Vector3(end_pos.x, role.getGameObject().transform.position.y, end_pos.z);
            actionManager.moveRole(role.getGameObject(), middle_pos, end_pos, role.move_speed);
            
            role.GoBoat(boat);
            boat.AddRole(role);
        }

        user_gui.sign = judger.Check();
        if (user_gui.sign != 1) {
            Restart();
        }
    }

    public void Restart() {
        start_land.Reset();
        end_land.Reset();
        boat.Reset();
        for (int i = 0; i < roles.Length; i++) {
            roles[i].Reset();
        }
    }

    public int Check() {
        return 1;
    }

    // public int Check() {
    //     int start_priest = (start_land.GetRoleNum())[0];
    //     int start_devil = (start_land.GetRoleNum())[1];
    //     int end_priest = (end_land.GetRoleNum())[0];
    //     int end_devil = (end_land.GetRoleNum())[1];

    //     if (end_priest + end_devil == 6)        // 获胜
    //         return 3;

    //     int[] boat_role_num = boat.GetRoleNum();
    //     if (boat.GetBoatSign() == 1) {          // 在开始岸和船上的角色
    //         start_priest += boat_role_num[0];
    //         start_devil += boat_role_num[1];
    //     } else {                                // 在结束岸和船上的角色
    //         end_priest += boat_role_num[0];
    //         end_devil += boat_role_num[1];
    //     }

    //     if ((start_priest > 0 && start_priest < start_devil) || (end_priest > 0 && end_priest < end_devil)) { //失败
    //         return 2;
    //     }

    //     return 1;                               //未完成
    // }
}