using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UFO;
public class UserGUI : MonoBehaviour {
    public int reset;
    GUIStyle style;
	GUIStyle buttonStyle;

    // v2-add
    ActionMode mode;

    // Start is called before the first frame update
    void Start() { 
        // 初始化为1，选择模式
        reset = 1;
        style = new GUIStyle();
		style.fontSize = 30;
		style.normal.textColor = Color.green;

		buttonStyle = new GUIStyle("button");
		buttonStyle.fontSize = 30;
		buttonStyle.normal.textColor = Color.green;
    }

    // Update is called once per frame
    void Update() {

    }

    // v2-modify
    private void OnGUI() {
        if(reset == 1) {
            if(GUI.Button(new Rect(100, 250, 200, 80), "KINEMATIC", buttonStyle)){
                mode = ActionMode.KINEMATIC;
                Director.getInstance().currentSceneController.setMode(mode);
                Director.getInstance().currentSceneController.Init();
                Director.getInstance().currentSceneController.setGameBegin(1);
                reset = 0;
                return;
            }else if(GUI.Button(new Rect(500, 250, 200, 80), "PHYSICS", buttonStyle)){
                mode = ActionMode.PHYSICS;
                Director.getInstance().currentSceneController.setMode(mode);
                Director.getInstance().currentSceneController.Init();
                Director.getInstance().currentSceneController.setGameBegin(1);
                reset = 0;
                return;
            } 
        }else if(reset == 0) {
            int round = Director.getInstance().currentSceneController.getSceneController().getRound();
            int total = Director.getInstance().currentSceneController.getSceneController().getTotal();
            int score = Director.getInstance().currentSceneController.getSceneController().getScore();
            //string text = "Round: " + userAction.getSceneController().GetRound().ToString() + "\nTotal:  " + total.ToString() + "\nScores:  " + score.ToString();
            string text = "Round: " + round.ToString() + "\nTotal:  " + total.ToString() + "\nScores:  " + score.ToString();
            GUI.Label(new Rect(10, 10, Screen.width, 50),text,style);      
        }
    }

}
