### 02-离散仿真引擎基础

主要文件为MineSweeper.cs，在sence中创建一个GameObject然后添加该脚本运行即可

[对应作业博客](https://blog.csdn.net/try17875864815/article/details/108587071)

> 实现功能：
> - 递归扫雷(点击区域九宫格内无雷则打开一片区域)
> - `-1`代表地雷，其余数字表示九宫格内雷的个数
> - 计时、重新开始
>
> 未实现
> - 鼠标右键标记雷/清除标记
> - 鼠标双击打开九宫格内非标记位置
>
> 基本展示如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200917092649876.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3RyeTE3ODc1ODY0ODE1,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200917092657116.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3RyeTE3ODc1ODY0ODE1,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200917092702934.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3RyeTE3ODc1ODY0ODE1,size_16,color_FFFFFF,t_70#pic_center)


